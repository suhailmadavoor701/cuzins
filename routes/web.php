<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\categoryController;
use App\Http\Controllers\productController;
use App\Http\Controllers\userController;
use App\Http\Controllers\checkoutController;
use App\Http\Controllers\orderController;
use App\Http\Controllers\orderlistController;
use App\Http\Controllers\Auth\AdminLoginController;

use App\Http\Controllers\whatsappController;
use App\Http\Controllers\posterController;



use App\Models\Category;
use App\Models\Product;

/*
|--------------------------------------------------------------------------
| Web Routes edited by suhail
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/',[userController::class,'product']);
	
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('admin/home', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');

Route::get('/adminsection', function () {

	$products=DB::table('products')
	->get()->count();
	$categories=DB::table('categories')
	->get()->count();

	$new=DB::table('products')
       ->where('section','new')
       ->get()->count();
	   $normal=DB::table('products')
       ->where('section','normal')
       ->get()->count();

    return view('users.admin',compact('categories','products','normal','new'));
})->middleware('is_admin');


Route::get('/usersproducts',function ()
{
	
	$cat = DB::table('categories')
	->orderBy('id','desc')
	->get();
	$categories=Category::all();
	$products = DB::table('products')
	->orderBy('id','desc')
	->get();
    return view('users.products',compact('products','cat', 'categories'));
	// return view('users.products');




});

Route::get('/', function()
{

     //posters
			$poster = DB::table('posters')
			->orderBy('id','desc')
			->get();
		
	 //category	
			$categories = $cat =Category::all();
	 //new_products
			$newproduct = DB::table('products')
			->where('section', 'new')
			->orderBy('id','desc')
			->get();


			$normal = Product::where('section', '=','normal')->paginate(5);
			

return view('users.index',compact('newproduct','cat','poster','normal', 'categories'));

});

Route::get('/contact',function()
{
	$categories =Category::all();
	 return view('users.contact',compact('categories'));

});


// user side


Route::resource('/product', productController::class);
Route::resource('/checkout', checkoutController::class)->middleware('auth');
Route::resource('/order', orderController::class)->middleware('auth');

Route::resource('/orderlist', orderlistController::class);

Route::get('/addtocart/{id}', [App\Http\Controllers\cartController::class, 'addToCart'])->middleware('auth');

Route::get('/cartindex',[App\Http\Controllers\cartController::class,'index'])->middleware('auth');

Route::get('/cartupdate/{id}',[App\Http\Controllers\cartController::class,'update']);
Route::delete('/cartdelete/{id}',[App\Http\Controllers\cartController::class,'destroy']);
Route::put('/cartupdate/{id}',[App\Http\Controllers\cartController::class,'update']);
Route::put('/clear',[App\Http\Controllers\cartController::class,'clear']);
Route::get('/productshow',[userController::class,'productshow']);
Route::get('/productsingleshow/{id}',[userController::class,'productsingleshow']);

Route::get('/productbycategory/{id}',[userController::class,'productsbycategory']);

Route::resource('/whatsapp', whatsappController::class);
Route::resource('/whatsapp', whatsappController::class);









// admim side
Route::resource('/category', categoryController::class)->middleware('is_admin');

Route::resource('/posters', posterController::class)->middleware('is_admin');

Route::get('/itemorders',[App\Http\Controllers\AdminController::class,'itemorders'])->middleware('is_admin');


Route::get('/orderupdate/{id}',[App\Http\Controllers\orderlistController::class,'update'])->middleware('is_admin');


Route::get('orderlist/{status?}',['AdminController','orders'])->middleware('is_admin');

Route::get('/delivered',[App\Http\Controllers\AdminController::class,'delivered'])->middleware('is_admin');


Route::delete('/orderdelete/{id}',[App\Http\Controllers\orderlistController::class,'destroy'])->middleware('is_admin');



