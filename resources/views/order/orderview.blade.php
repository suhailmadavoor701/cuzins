@extends('layouts.admin')


@section('content')
   @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


    @section('content')
          @if(Session::has('status'))
<p class="alert alert-info">{{ Session::get('status') }}</p>
@endif
 
 <section class="content-header">
      <h1>
      Order view
        {{-- <small>preview of simple tables</small> --}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Order</a></li>
        <li class="active">View</li>
      </ol>
    </section>
          <!-- /.box -->

         
      
 
    <div class="row">
       <div style="padding-right: 10px;padding-left: 10px;">
        <div class="col-xs-12">
          <div class="box">
            
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  {{-- <th>Category</th> --}}
                  <th>Order Id</th>
                  
                  <th>Product Name</th>
                  <th>Quantity</th>
                  <th>Size</th>
                  <th>price</th>
                  
                
                  
                  <th>Action</th>
                </tr>
                

                @foreach ($order->items as $value) 

       

                
                 <tr>
                  
                  <td>{{ $order->id }}</td>
                  <td>{{ $value->name }} </td>
                  <td>{{  $value->pivot->quantity }}</td>
                  <td>{{ $value->pivot->size }}</td>


                  

                  
                  <td>{{  $value->pivot->price }}</td>
                  
                  


                   @endforeach
                    <td>

                      <div class="form-group">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Category</label>
                        <form action="/orderupdate/{{$order->id }}"  method="get">
                                    <select class="form-control" name="name">
                                      <option class="form-control" >Select The Category</option>
                                      



                                        <option href="" required="" name="name"  class="form-control" value="completed">Completed</option>

                                        <option href="" required="" name="name"  class="form-control" value="pending">Pending</option>
                                      
                                 </select>
                             </div>
                </div>
                                        <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
                                          {{--   <a href="/orderupdate/{{$order->id }}"> <button onclick="if (confirm('Are you sure..?')) commentDelete(1); return false" class="btn btn-warning"  type="submit">Completed Order</button></a> --}}
                                           
                                          </form>

                                        </td>
                  
                






                   
          
                </tr>
               
              </table>
            </div>
            
          </div>
        </div>
      </div>
    </div>
          
        </div>
      </div>

@endsection





