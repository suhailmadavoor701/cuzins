@extends('layouts.admin')


@section('content')
   @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


    @section('content')
          @if(Session::has('status'))
<p class="alert alert-info">{{ Session::get('status') }}</p>
@endif
 
 <section class="content-header">
      <h1>
      Order List
        {{-- <small>preview of simple tables</small> --}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Order</a></li>
        <li class="active">List</li>
      </ol>
    </section>
          <!-- /.box -->

         
      
    </section>
     <div class="row">
       <div style="padding-right: 10px;padding-left: 10px;">
        <div class="col-xs-12">
          <div class="box">
            
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  {{-- <th>Category</th> --}}
                  <th>Name</th>
                  <th>Address</th>
                  <th>State</th>
                  <th>City</th>
                  <th>Post</th>
                  <th>Number</th>
                  <th>User Id</th>
                  <th>Grand Total</th>
                  <th>Item Quantity</th>
                  <th>Shipping Status</th>
                  
                  <th colspan="2" style="text-align: center;">Action</th>
                </tr>
                @foreach($order as $order)
                 <tr>
                  
                  <td>{{ $order->shipping_fullname }}</td>
                  <td>{{ $order->shipping_address }}</td>
                  
                  <td>{{ $order->shipping_state }}</td>
                  
                  <td>{{ $order->shipping_city }}</td>
                  <td>{{ $order->shipping_zipcode }}</td>
                  <td>{{ $order->shipping_phone }}</td>
                  <td>{{ $order->user_id }}</td>
                  <td>{{ $order->grand_total }}</td>
                  <td>{{ $order->item_count }}</td>
                  <td>{{ $order->status }}</td>






                   
                  
                <td><a href="/orderlist/{{ $order->id }}" class="btn btn-warning">View</a></td>

                              <td>
                                          <form action="/orderdelete/{{$order->id}}" method="post">
                                            {{csrf_field()}}
                                            <input name="_method" type="hidden" value="DELETE">
                                           <button onclick="if (confirm('Are you sure..?')) commentDelete(1); return false" class="btn btn-warning"  type="submit">Delete</button>
                                          </form>

                                        </td>
                </tr>
                @endforeach
              </table>
            </div>
            
          </div>
        </div>
      </div>
    </div>
          
        </div>
      </div>

@endsection





