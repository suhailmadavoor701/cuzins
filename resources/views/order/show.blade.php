@extends('layouts.admin')


@section('content')
   @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


    @section('content')
          @if(Session::has('status'))
<p class="alert alert-info">{{ Session::get('status') }}</p>
@endif
 
 <section class="content-header">
      <h1>
        Blog List
        {{-- <small>preview of simple tables</small> --}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Banner</a></li>
        <li class="active">Create</li>
      </ol>

         
      
    </section>
       <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools">
               {{--  <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div> --}}
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  {{-- <th>Category</th> --}}
                  <th>Name</th>
                  <th>Title</th>
                  <th>Category</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th colspan="2" style="text-align: center;">Action</th>
                </tr>
                @foreach($banner as $ban)
                 <tr>
                  {{-- <td>{{ $ban->categories_id }}</td> --}}
                  <td>{{ $ban->name }}</td>
                  <td>{{ $ban->title }}</td>
                  <td>{{ $ban->description }}</td>
                  <td>{{ $ban->name }}</td>

                   <td><img height="50px" width="50px" src="{{ asset('uploads/blogs/'.$ban->image) }}"></td>
                  
                <td><a href="{{action('blogController@edit',$ban->id)}}" class="btn btn-warning">Edit</a></td>

                              <td>
                                          <form action="{{action('blogController@destroy',$ban->id)}}" method="post">
                                            {{csrf_field()}}
                                            <input name="_method" type="hidden" value="DELETE">
                                           <button onclick="if (confirm('Are you sure..?')) commentDelete(1); return false" class="btn btn-warning"  type="submit">Delete</button>
                                          </form>

                                        </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

@endsection





