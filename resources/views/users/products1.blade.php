@extends('layouts.users')
@section('content')
<main class="main ">
			<div class="page-content mb-10 pb-6">
				<div class="container">
					<div class="row mt-8 gutter-lg">
						<aside class="col-lg-3 sidebar sidebar-fixed sticky-sidebar-wrapper">
							<!-- <div class="sidebar-overlay">
								<a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
							</div> -->
							<!-- <a href="#" class="sidebar-toggle"><i class="fas fa-chevron-right"></i></a> -->
							<div class="sidebar-content">
								<div class="sticky-sidebar">
									<div class="service-list mb-4">
                                    <!-- <h3 style="padding-right: 120px;">Categories</h3> -->
										<div class="icon-box icon-box-side icon-box3">
										    
											<div class="icon-box-content">
                                                <h5>Catgories</h5>
                                            @foreach($cat as $ca)
												<h4 class="icon-box-title text-capitalize"><a href="/productbycategory/{{$ca->id}}">{{$ca->name}}</a></h4>
												<!-- <p>We secure payment!</p> -->
                                                @endforeach
											</div>
                                            
										</div>
									
									
									</div>
								
								</div>
							</div>
						</aside>
						<div class="col-lg-9">
							  
                         
                     
                        <section class="banner-group mt-4">
                    <div class="container">
                        <h2 class="title d-none">Banner Group</h2>

                        @foreach($products as $post)

                        <div class="row justify-content-center">
                            <div class="col-lg-4 col-sm-6 mb-4">
                                <div class="banner banner-3 banner-fixed banner-radius content-middle appear-animate"
                                    data-animation-options="{'name': 'fadeInLeftShorter', 'duration': '1s', 'delay': '.2s'}">
                                    <figure>
                                        <img src="uploads/products/{{$post->image}}" alt="banner" width="380"
                                            height="207" style="background-color: #836648;" />
                                    </figure>
                                
                                </div>
                            </div>
                           
                           
                        </div>
                        @endforeach


                    </div>
                </section>
						

						</div>
					</div>
				</div>
			</div>
		</main>

@endsection