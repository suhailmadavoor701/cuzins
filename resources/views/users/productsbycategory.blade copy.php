@extends('layouts.users')
@section('content')
<main class="main">
    <nav class="breadcrumb-nav">
    </nav>
    <div class="page-content with-sidebar">

        <div class="container">
            <div class="row gutter-lg">
                <div class="col-lg-9 col-sm-12">
                    
                    <!-- deleted stuff is being shown brother u there? there is no div element with that class-->

                    <div class="row">
                        <div class="col-sm-12">
                            @foreach($products as $prod)
                            <div class="col-sm-3">
                                <article class="post mb-3">
                                    <figure class="post-media overlay-zoom">
                                        <a href="">
                                            <img src="/uploads/products/{{$prod->image}}" alt="post" />
                                        </a>
                                    </figure>
                                    <div class="post-details">

                                     <h6><a href="">{{$prod->name}}</a></h6>
                                        <div class="product-price">
                                            <ins class="new-price">{{$prod->oprize}}</ins><del class="old-price">{{$prod->aprize}}</del>
                                        </div>

                                    </div>
                                </article>
                            </div>
                            @endforeach
                        </div>

                    </div>





                </div>

            </div>
        </div>
    </div>
</main>
@endsection