@extends('layouts.users')

@section('content')
		<!-- header-section (default-header) - start
		================================================== -->
	

 

		<!-- shopping-cart-section start
		================================================== -->

		<section id="shopping-cart-section" class="shopping-cart-section sec-ptb-60 clearfix">
			<div class="container">

				<!-- shopping-cart-table - start -->
			
               
               
               
				<div class="shopping-cart-table mb-60">
					<table class="table table-bordered mb-30">
						
						<thead>
							<tr>
								<th colspan="2">Name</th>
								<th scope="col">Price</th>
		
								<th scope="col">Quantity & size</th>
								<th scope="col">Total</th>
							</tr>
						</thead>
						@foreach($cartitems as $cartItem)
						<tbody>
							<tr>



								<td class="text-left" colspan="2">
									
									
									<span class="image-container float-left">
										<img src="{{asset('uploads/products/'.$cartItem->attributes->image)}}"  value="" alt="image_not_found">
										<br>

									</span>
									<h3>{{$cartItem->name}}</h3>

									<br>
										
									
									<ul  class="clearfix">
										
										  <li style="padding-top:80px;"><form  action="/cartdelete/{{$cartItem->id}}" method="post">
                                            {{csrf_field()}}
                                            <input class="flaticon-dustbin" name="_method" type="hidden" value="DELETE">
                                           <button  onclick="if (confirm('Are you sure..?')) commentDelete(1); return false" class="btn btn-danger"  type="submit">Delete</button>
                                          </form><li>
									</ul>

									
								</td>
								<td class="item-price text-center" id="myBox1" >{{$cartItem->price}}</td>
								

                         
								
			  		 <td ><form action="/cartupdate/{{$cartItem->id}}" method="post">
								 	{{csrf_field()}}
								 	<input name="_method" type="hidden" value="PUT">
								
								 	<div>
								 	@if($cartItem->attributes->has('size') )
								 		
                                        
                                       <select  name="size"> 
                                       	   <option value="{{$cartItem->attributes->size}}">{{$cartItem->attributes->size}}</option>
                                          <option>Small</option>
                                          <option>Medium</option>
                                          <option>Large</option>


                                       </select>
 
                                     @endif
                                         
								 	
								 </div>
							

								 	<input class="item-quantity text-center" type="number" placeholder="quantity" name="qty"  value="{{$cartItem->quantity}}">

								 	<input type="submit" class="btn btn-sm btn-defualt" value="ok">
								 </form>
								 </td>
								 							
								<td id="result" class="total-price text-center">{{ $cartItem->getPriceSum()}}</td>
							</tr>
						</tbody>
						@endforeach
					</table>

					<div class="row">
						<div class="col-lg-6 col-md-4 col-sm-12">
							<a href="{{url('/')}}" class="continue-btn">continue shopping</a>
						</div>
						<!-- <div class="col-lg-3 col-md-4 col-sm-12">
							<div class="text-right">
								<a href="#!" class="update-btn">update shopping cart</a>
							</div>
						</div> -->
						<div class="col-lg-3 col-md-4 col-sm-12">
							<div class="text-right">
								<a href="/clear" class="clear-btn">clear shopping cart</a>
							</div>
						</div>
					</div>
				</div>
				
				<!-- shopping-cart-table - end -->

				
					<!-- shipping-estimate - end -->

					<!-- grand-total-price - start -->
					<div class="col-lg-6 col-md-6 col-sm-12">
						<div class="grand-total-price">

							<div class="money-list ul-li-block">
								<ul class="clearfix">
									<li>subtotal <span class="float-right">{{$cartTotal = Cart::getSubTotal()}}</span></li><br>
                                               <h4></h4>
									
									<li>No of Items<span class="float-right">{{$cartTotalQuantity = Cart::getTotalQuantity()}}</span></li>
									
								</ul>
							</div>
							<h2 class="total-price mb-30">grand total <strong>{{ $cartTotal = Cart::getTotal()}}</strong></h2>
							<a href="/checkout" class="proceed-btn">Proceed to checkout</a>

						</div>
					</div>
					<!-- grand-total-price - end -->
				</div>

			</div>
		</section>
		<!-- shopping-cart-section end
		================================================== -->





		<!-- footer-section (default-footer) - start
		================================================== -->
	
		@endsection
		