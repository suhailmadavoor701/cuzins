@extends('layouts.users')


@section('title','Home')

@section('content')
    <section id="slider-section" class="slider-section clearfix">

            <!-- slider-item - start -->
            <div class="slider-item" style="background-image: url({{URL::asset('users/assets/images/slider/16-tools-1.jpg')}}">
                <div class="container">
                    <div class="row justify-content-lg-end justify-content-md-center">

                        <div class="col-lg-6 col-md-8 col-sm-12">
                            <div class="slider-content">
                                <h1 class="color-white" >The Most <span>Advanced Drill</span> Ever Made</h1>
                                <p class="color-white mb-30">Ultimate performance, unbelievable power, leading edge technology</p>
                                <a href="#!" class="custom-btn bg-yellow">shop now</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- slider-item - end -->

            <!-- slider-item - start -->
            <div class="slider-item" style="background-image: url({{URL::asset('users/assets/images/slider/16-tools-2.jpg')}}">
                <div class="container">
                    <div class="row justify-content-lg-end justify-content-md-center">

                        <div class="col-lg-6 col-md-8 col-sm-12">
                            <div class="slider-content">
                                <h1 class="color-white">The Most <span>Advanced Drill</span> Ever Made</h1>
                                <p class="color-white mb-30">Ultimate performance, unbelievable power, leading edge technology</p>
                                <a href="#!" class="custom-btn bg-yellow">shop now</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- slider-item - end -->

            <!-- slider-item - start -->
            <div class="slider-item" style="background-image: url({{URL::asset('users/assets/images/slider/16-tools-3.jpg')}}">
                <div class="container">
                    <div class="row justify-content-lg-end justify-content-md-center">

                        <div class="col-lg-6 col-md-8 col-sm-12">
                            <div class="slider-content">
                                <h1 class="color-white">The Most <span>Advanced Drill</span> Ever Made</h1>
                                <p class="color-white mb-30">Ultimate performance, unbelievable power, leading edge technology</p>
                                <a href="#!" class="custom-btn bg-yellow">shop now</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- slider-item - end -->

        </section>
        <!-- slider-section - end
        ================================================== -->




        <!-- category-section - start
        ================================================== -->
       
        <!-- category-section - end
        ================================================== -->





        <!-- featured-section - start
        ================================================== -->
        <div id="featured-section" class="featured-section sec-ptb-60 clearfix">
            <div class="featured-container">
                <div class="container">
                    <!-- <ul class="nav digital-featured-nav">
                        <li><a class="active" data-toggle="tab" href="#featured">featured</a></li>
                        <li><a data-toggle="tab" href="#best-seller">Best Seller</a></li>
                        <li><a data-toggle="tab" href="#top-rated">Top Rated</a></li>
                    </ul> -->
                </div>

                <div class="tab-content">
                    <!-- tab-pane (featured) - start -->
                    <div id="featured" class="tab-pane fade in active show">
                        <div class="container">
                            <center><h6 style="font-size:  24px;font-style: bold">BEST PRODUCTS</h6></center><br>
                            <div class="row">

                                @foreach($product as $pro)
                                      
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                    <div class="product-item">
                                        <div class="post-labels">
                                            <ul class="clearfix">
                                                <!-- <li class="bg-primary">new</li> -->
                                                {{-- <li class="bg-danger">-50%</li> --}}
                                            </ul>
                                        </div>
                                        <div class="image-container">
                                            <img name="img"  src="{{ asset('uploads/products/'.$pro->image) }}" alt="image_not_found">
                                            
                                        </div>
                                        <div class="item-content text-center">
                                            <a href="#!" name="name" class="item-title">{{ $pro->name }}</a>
                                            <div class="item-price">
                                                <strong name="prize" class="color-black">{{ $pro->oprize }}</strong>
                                                <del>{{ $pro->aprize }}</del>
                                            </div>
                                             <a href="/addtocart/{{$pro->id}}" class="add-to-cart">
                                                <i class="flaticon-shopping-basket"></i>
                                                add to cart
                                            </a>
                                        </div>
                                      
                                    </div>
                                </div>
                                @endforeach


                                
                               
                           
                                 
                                

                            </div>
                        </div>
                    </div>
                    <!-- tab-pane (featured) - end -->

                    <!-- tab-pane (best-seller) - start -->
                
                        

                                

                            </div>
                        </div>
                    </div>
                    <!-- tab-pane (top-rated) - start -->
                </div>

            </div>
        </div>
        <!-- featured-section - end
        ================================================== -->






@endsection