@extends('layouts.users')
@section('content')


	<section id="product-section" class="product-section sec-ptb-100 clearfix">
			<div class="container">
				<div class="row justify-content-md-center">

					<!-- sidebar-section - start -->
					<div class="col-lg-3 col-md-4 col-sm-12">
						<div class="sidebar-section">

							<!-- category-list - start -->
							<div class="sidebar-item category-list ul-li-block mb-30">
								<div class="sidebar-title">
									<h2>Categories</h2>
								</div>
								<ul class="clearfix">
									@foreach($cat as $cat)
															<li><a href="productsbycategory/{{ @$cat->id }}">{{ $cat->name }}</a></li>
															@endforeach
									{{-- <li><a href="#!">New Arrivals <span class="float-right"></span></a></li> --}}
								
								</ul>
							</div>
							

					

						
						

						


						
						</div>
					</div>
					<!-- sidebar-section - end -->

					<!-- product-grid-section - start -->
					<div class="col-lg-9 col-md-10 col-sm-12">
						<div class="product-grid-section">
							<div class="row">

							

								<!-- tab-content - start -->
								<div class="col-lg-12 col-md-12 col-sm-12">
									<div class="tab-content mb-60">

										<div id="grid-style" class="tab-pane fade in active show">
											<div class="row">

												  @foreach($product as $pro)
                                      
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                    <div class="product-item">
                                        <div class="post-labels">
                                            <ul class="clearfix">
                                                <!-- <li class="bg-primary">new</li> -->
                                                {{-- <li class="bg-danger">-50%</li> --}}
                                            </ul>
                                        </div>
                                        <div class="image-container">
                                            <img name="img"  src="{{ asset('uploads/products/'.$pro->image) }}" alt="image_not_found">
                                            
                                        </div>
                                        <div class="item-content text-center">
                                            <a href="productsingleshow/{{ $pro->id }}" name="name" class="item-title">{{ $pro->name }}</a>
                                            <div class="item-price">
                                                <strong name="prize" class="color-black">{{ $pro->oprize }}</strong>
                                                <del>{{ $pro->aprize }}</del>
                                            </div>
                                             <a href="/addtocart/{{$pro->id}}" class="add-to-cart">
                                                <i class="flaticon-shopping-basket"></i>
                                                add to cart
                                            </a>
                                        </div>
                                      
                                    </div>
                                </div>
												@endforeach
												<!-- product-item - end -->




									
												
											</div>	
										</div>

									
						</div>
					</div>
					<!-- product-grid-section - end -->
					
				</div>
			</div>
		</section>

@endsection