@extends('layouts.users')
@section('content')


	<!-- product-details-section - start
		================================================== -->
		<section id="product-details-section" class="product-details-section sec-ptb-60 clearfix">
			<div class="container">

				<!-- product-details - start -->
				<div class="product-details mb-60">
					<div class="row justify-content-md-center">

						<!-- product-details-carousel - start -->
						<div class="col-lg-6 col-md-10 col-sm-12">
							<div class="product-details-carousel">
								<div class="slider-for">
									<div class="item">
										<img style="height: 500px;" src="{{ asset('uploads/products/'.$product->image) }}" alt="image_not_found">
									</div>
								
								</div>

							
							</div>
						</div>
						<!-- product-details-carousel - end -->

						<!-- product-details-content - start -->
						<div class="col-lg-6 col-md-10 col-sm-12">
							<div class="product-details-content">

								<div class="product-code ul-li mb-30">
									{{-- <ul class="clearfix">
										<li>SKU: SK68</li>
										<li>Availability: <span class="text-success"></span></li>
									</ul> --}}
								</div>
								<div class="product-title mb-30">
									{{-- <div class="rateing-star">
										<ul>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
										</ul>
							
									</div> --}}
										<h2>{{ $product->name }}</h2>									
										<h3>Offer Prize:{{ $product->oprize }}</h3>
										<h6>Actuall Prize:{{ $product->aprize }}</h6>
								</div>
								<p class="mb-30">
									<p>{{ $product->description }}</p>
								</p>
								
							
								
								<div class="item-btns-group ul-li clearfix mb-30">
									<ul class="clearfix">
										<li>
											<a href="/addtocart/{{$product->id}}" class="add-to-cart">
												<i class="flaticon-shopper"></i>
												add to cart
											</a>
										</li>
									</ul>
								</div>
								
								
							</div>
						</div>
						<!-- product-details-content - end -->

					</div>
				</div>


@endsection