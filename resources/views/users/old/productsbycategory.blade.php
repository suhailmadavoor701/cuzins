@extends('layouts.users')
@section('content')


	<div id="featured-section" class="featured-section sec-ptb-60 clearfix">
			<div class="featured-container">
				<div class="container">
					<!-- <ul class="nav digital-featured-nav">
						<li><a class="active" data-toggle="tab" href="#featured">featured</a></li>
						<li><a data-toggle="tab" href="#best-seller">Best Seller</a></li>
						<li><a data-toggle="tab" href="#top-rated">Top Rated</a></li>
					</ul> -->
				</div>

				<div class="tab-content">
					<!-- tab-pane (featured) - start -->
					<div id="featured" class="tab-pane fade in active show">
						<div class="container">
							<center><h6 style="font-size:  24px;font-style: bold">BEST PRODUCTS</h6></center><br>
							
							<div class="row">
                                  @foreach($banner as $pro)
                                      
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                    <div class="product-item">
                                        <div class="post-labels">
                                            <ul class="clearfix">
                                                <!-- <li class="bg-primary">new</li> -->
                                                {{-- <li class="bg-danger">-50%</li> --}}
                                            </ul>
                                        </div>
                                        <div class="image-container">
                                            <img name="img"  src="{{ asset('uploads/products/'.$pro->image) }}" alt="image_not_found">
                                            
                                        </div>
                                        <div class="item-content text-center">
                                            <a href="productsingleshow/{{ $pro->id }}" name="name" class="item-title">{{ $pro->name }}</a>
                                            <div class="item-price">
                                                <strong name="prize" class="color-black">{{ $pro->oprize }}</strong>
                                                <del>{{ $pro->aprize }}</del>
                                            </div>
                                             <a href="/addtocart/{{$pro->id}}" class="add-to-cart">
                                                <i class="flaticon-shopping-basket"></i>
                                                add to cart
                                            </a>
                                        </div>
                                      
                                    </div>
                                </div>
                                @endforeach
							</div>

						</div>
					</div>
					<!-- tab-pane (featured) - end -->

					<!-- tab-pane (best-seller) - start -->
				
						

								

							</div>
						</div>
					</div>
					<!-- tab-pane (top-rated) - start -->
				</div>

			</div>
		</div>

@endsection