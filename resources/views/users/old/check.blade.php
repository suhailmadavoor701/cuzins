@extends('layouts.users')


@section('title','Checkout')

@section('content')


   @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


          @if(Session::has('status'))
<p class="alert alert-info">{{ Session::get('status') }}</p>
@endif


		<!-- checkout-section start
		================================================== -->
		<section id="checkout-section" class="checkout-section sec-ptb-60 clearfix">
			<div class="container">
				<div class="row justify-content-md-center">

					<div class="col-lg-8 col-md-12 col-sm-12">

						<!-- sign-in-container - start -->
						<div class="sign-in-container">
							<!-- <a href="#!" class="sign-in-btn">sign in</a> -->
						</div>
						<!-- sign-in-container - end -->

						<div class="checkout-content">
							<ul class="nav checkout-nav mb-60">
								<li>
									<a class="active" data-toggle="tab" href="#menu1">
										<!-- <span>1</span> -->
										<!-- <strong>shipping</strong> -->
									</a>
								</li>
								<!-- <li>
									<a data-toggle="tab" href="#menu2">
										<span>2</span>
										<strong>Review & Payments</strong>
									</a>
								</li> -->
							</ul>
							<div class="tab-content">

								<div id="menu1" class="tab-pane fade in active show">

									<div class="section-title">
										<h2>shipping address</h2>
									</div>

									<form action="/order" method="post">
										{{csrf_field()}}
										<div class="row">

											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-item">
													<span class="input-title">First Name<sup>*</sup></span>
													<input type="text" required="" name="name">
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-item">
													<span class="input-title">State<sup>*</sup></span>
													<input type="text" required="" name="state">
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-item">
													<span class="input-title">City<sup>*</sup></span>
													<input type="text" required="" name="city">
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-item">
													<span class="input-title">Address<sup>*</sup></span>
													<input type="text" required="" name="address">
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-item">
													<span class="input-title">Post<sup>*</sup></span>
													<input type="number" required="" name="post">
												</div>
											</div>
											
											

											
											
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="form-item">
													<span class="input-title">Phone<sup>*</sup></span>
													<input required="" type="number" name="phone">
												</div>
											</div>
											
										</div>
										{{-- <h4>Payment option</h4> --}}

     <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="payment_method" id="" value="cash_on_delivery">
            Cash on delivery

        </label>

    </div>

    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="payment_method" id="" value="paypal">
            Paypal

        </label>

    </div>
 								{{-- <a href="" class="next-btn custom-btn bg-past">next</a> --}}
					{{-- 		<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div> --}}
            


       


              		<div class="box-footer">
                <button id="clickButton" 
				class="btn btn-primary" type="submit" class="btn btn-primary">
               
                      Place The order</a></button>
             </div>
							
									</form>

								</div>

								

							</div>
						</div>
					</div>




					

                   
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="checkout-sidebar-section">

							<div class="order-summary mb-30 clearfix">
								<div class="section-title">
									<h2>Order Summary</h2>
								</div>

								@foreach($cartitems as $cartItem)

								<span class="item-amount mb-15"></span>
								<div class="item-summary">
									<div class="image-container">
										<img src="{{asset('uploads/products/'.$cartItem->attributes->image)}}" alt="image_not_found">
									</div>
									<div class="item-content">
										<h3 class="title-text">{{$cartItem->name}}</h3>
										<span class="qty-text mb-15">Qty: <small>{{$cartItem->quantity}}</small></span>
										<div class="item-price">
											<strong class="color-black"></strong>
										</div>
									</div>

								</div>
								@endforeach
							</div>
						<h6 class="total-price mb-30">grand total <strong>{{ $cartTotal = Cart::getTotal()}}</strong></h6>

							<div class="shipping-method clearfix">
								<div class="section-title">
									<h2></h2>
								</div>

							</div>
							
						</div>
					</div>

				</div>
			</div>
		</section>

		<!-- checkout-section end
		================================================== -->



	@endsection