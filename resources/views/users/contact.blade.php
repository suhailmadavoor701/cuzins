@extends('layouts.users')
@section('content')
<main class="main">
            <nav class="breadcrumb-nav">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="demo1.html"><i class="d-icon-home"></i></a></li>
                        <li>Contact Us</li>
                    </ul>
                </div>
            </nav>
            <div class="page-header" style="background-image: url({{asset('users/images/page-header/contact-us.jpg')}})">
                <h1 class="page-title font-weight-bold text-capitalize ls-l">Contact Us</h1>
            </div>
            <div class="page-content mt-10 pt-7">
                <section class="contact-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-6 ls-m mb-4">
                                <div class="grey-section d-flex align-items-center h-100">
                                    <div>
                                        <h4 class="mb-2 text-capitalize">Address</h4>
                                        <p>Kunnamangalam, Karanthur,<br> Oppo:Palakkal Petrol Pumb</p>

                                        <h4 class="mb-2 text-capitalize">Phone Number</h4>
                                        <p>
                                        <a target="_blank" href="tel:919745177722">974 5177 722</a>
                                            
                                        

                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-8 col-sm-6 d-flex align-items-center mb-4">
                                <div class="w-100">
                                <div style="" > <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3912.4159335023196!2d75.86653041429074!3d11.304278952320708!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba65dfe896596f3%3A0xc330267cff28efe9!2sCuzins%20Kitchenware!5e0!3m2!1sen!2sin!4v1619169224927!5m2!1sen!2sin" width="800" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End About Section-->

                <section class="store-section mt-6 pt-10 pb-8">
                    <div class="container">
                        <h2 class="title title-center mb-7 text-normal">Our store</h2>
                        <div class="row cols-sm-2 cols-lg-4">
                            <div class="store">
                                <figure class="banner-radius">
                                    <img src="{{asset('users/images/subpages/store-1.jpg')}}" alt="store" width="280" height="280">
                                    <h4 class="overlay-visible">Kunnamangalam, Calicut</h4>
                                    
                                    <div class="overlay overlay-transparent">
                                        
                                        <a class="mt-8" href=""><p>Kunnamangalam, Karanthur,<br> Oppo:Palakkal Petrol Pumb</p></a>
                                        <a target="_blank" href="tel:+919745177722">974 5177 722</a>
                                        <div class="social-links mt-1">
                                            <a href="#" class="social-link social-facebook fab fa-facebook-f"></a>
                                            <a href="#" class="social-link social-twitter fab fa-twitter"></a>
                                            <a href="#" class="social-link social-linkedin fab fa-linkedin-in"></a>
                                        </div>
                                    </div>
                                </figure>
                            </div>
                   
                  
                        </div>
                    </div>
                </section>
                <!-- End Store Section -->

                <!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
                <!-- <div class="grey-section google-map" id="" style="height: 386px"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3912.411149138987!2d75.86653411429076!3d11.30462945231399!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba65cef1a447fb5%3A0x5c73c322327dab2f!2sIndian%20Oil%3A%20Palakkal%20Petroleum%20fuel%20%40call!5e0!3m2!1sen!2sin!4v1619169160315!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div> -->
                <!-- End Map Section -->
            </div>
        </main>
@endsection