@extends('layouts.users')
@section('content')
<main class="main">
            <div class="page-content">
                <section class="intro-section">
                    <div class="owl-carousel owl-theme row owl-dot-inner owl-dot-white intro-slider animation-slider cols-1 gutter-no"
                        data-owl-options="{
                        'nav': false,
                        'dots': true,
                        'loop': false,
                        'items': 1,
                        'autoplay': false,
                        'autoplayTimeout': 8000
                    }">
                        <div class="banner banner-fixed intro-slide1" style="background-color: #46b2e8;">
                            <figure>
                                
                                    <div style="background-color:#34ace5;"></div>
                            </figure>
                            <div class="container">
                                <div class="banner-content y-50">
                                    <h4 class="banner-subtitle font-weight-bold ls-l">
                                        <span class="d-inline-block slide-animate"
                                            data-animation-options="{'name': 'fadeInRightShorter', 'duration': '1s', 'delay': '.2s'}">Buy
                                            2 Get</span>
                                        <span class="d-inline-block label-star bg-white text-primary slide-animate"
                                            data-animation-options="{'name': 'fadeInRightShorter', 'duration': '1s', 'delay': '.4s'}">1
                                            Free</span>
                                    </h4>
                                    <h2 class="banner-title font-weight-bold text-white lh-1 ls-md slide-animate"
                                        data-animation-options="{'name': 'fadeInUpShorter', 'duration': '1.2s', 'delay': '1s'}">
                                        Dream Kitchen</h2>
                                    <h3 class="font-weight-normal lh-1 ls-l text-white slide-animate"
                                        data-animation-options="{'name': 'fadeInUpShorter', 'duration': '1.2s', 'delay': '1s'}">
                                        Collection</h3>
                                    <p class="slide-animate text-white ls-s mb-7"
                                        data-animation-options="{'name': 'fadeInUpShorter', 'duration': '1.2s', 'delay': '1s'}">
                                        </p>
                                    <a href="/products" class="btn btn-dark btn-rounded slide-animate"
                                        data-animation-options="{'name': 'fadeInUpShorter', 'duration': '1s', 'delay': '1.8s'}">Shop
                                        Now<i class="d-icon-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                     
                     
                    </div>

                    <div class="container mt-6 appear-animate">
                        <div class="service-list">
                            <div class="owl-carousel owl-theme row cols-lg-3 cols-sm-2 cols-1" data-owl-options="{
                                    'items': 3,
                                    'nav': false,
                                    'dots': false,
                                    'loop': true,
                                    'autoplay': false,
                                    'autoplayTimeout': 5000,
                                    'responsive': {
                                        '0': {
                                            'items': 1
                                        },
                                        '576': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3,
                                            'loop': false
                                        }
                                    }
                                }">
                                <div class="icon-box icon-box-side icon-box1 appear-animate" data-animation-options="{
                                        'name': 'fadeInRightShorter',
                                        'delay': '.3s'
                                    }">
                                    <i class="icon-box-icon d-icon-truck"></i>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title text-capitalize ls-normal lh-1">Free Shipping &amp;
                                            Return
                                        </h4>
                                        <p class="ls-s lh-1">Free shipping on orders over $99</p>
                                    </div>
                                </div>

                                <div class="icon-box icon-box-side icon-box2 appear-animate" data-animation-options="{
                                        'name': 'fadeInRightShorter',
                                        'delay': '.4s'
                                    }">
                                    <i class="icon-box-icon d-icon-service"></i>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title text-capitalize ls-normal lh-1">Customer Support 24/7
                                        </h4>
                                        <p class="ls-s lh-1">Instant access to perfect support</p>
                                    </div>
                                </div>

                                <div class="icon-box icon-box-side icon-box3 appear-animate" data-animation-options="{
                                        'name': 'fadeInRightShorter',
                                        'delay': '.5s'
                                    }">
                                    <i class="icon-box-icon d-icon-secure"></i>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title text-capitalize ls-normal lh-1">100% Secure Payment
                                        </h4>
                                        <p class="ls-s lh-1">We ensure secure payment!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>



                <section class="banner-group mt-4">
                    <div class="container">
                        <h2 class="title d-none">Banner Group</h2>

                        @foreach($poster as $post)

                        <div class="row justify-content-center">
                            <div class="col-lg-4 col-sm-6 mb-4">
                                <div class="banner banner-3 banner-fixed banner-radius content-middle appear-animate"
                                    data-animation-options="{'name': 'fadeInLeftShorter', 'duration': '1s', 'delay': '.2s'}">
                                    <figure>
                                        <img src="uploads/posters/{{$post->image}}" alt="banner" width="380"
                                            height="207" style="background-color: #836648;" />
                                    </figure>
                                
                                </div>
                            </div>
                           
                           
                        </div>
                        @endforeach


                    </div>
                </section>

                <section class="pt-10 mt-7 appear-animate" data-animation-options="{
                    'delay': '.3s'
                }">
                    <div class="container">
                        <h2 class="title title-center mb-5">Our Categories</h2>
                        <div class="row">
                            @foreach($cat as $category)
                            <div class="col-xs-6 col-lg-3 mb-4">
                                <div class="category category-default1 category-absolute banner-radius overlay-zoom">
                                    <a href="">
                                        <figure class="category-media">
                                            <img src="uploads/category/{{$category->image}}" alt="category" width="280"
                                                height="280" style="background-color: #8c8c8d;" />
                                        </figure>
                                    </a>
                                    <div class="category-content">
                                        <h4 class="category-name font-weight-bold ls-l"><a href="/productbycategory/{{$category->id}}">{{$category->name}}</a></h4>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                           
                         
                           
                        </div>
                    </div>
                </section>

                <section class="product-wrapper container appear-animate mt-6 mt-md-10 pt-4 pb-8" data-animation-options="{
                    'delay': '.3s'
                }">
                    <h2 class="title title-center mb-5">New Products</h2>
                    <div class="owl-carousel owl-theme row owl-nav-full cols-2 cols-md-3 cols-lg-4" data-owl-options="{
                        'items': 5,
                        'nav': false,
                        'loop': false,
                        'dots': true,
                        'margin': 20,
                        'responsive': {
                            '0': {
                                'items': 2
                            },
                            '768': {
                                'items': 3
                            },
                            '992': {
                                'items': 4,
                                'dots': false,
                                'nav': true
                            }
                        }
                    }">
                        
                        @foreach($newproduct as $new)
                        <div class="product text-center">
                            <figure class="product-media">
                                <a href="product.html">
                                    <img src="uploads/products/{{$new->image}}" alt="Blue Pinafore Denim Dress"
                                        width="280" height="315" style="background-color: #f2f3f5;" />
                                </a>

                                <div class="product-label-group">
                                    <label class="product-label label-new">new</label>
                                </div>
                                <!-- <div class="product-action">
                                    <a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
                                </div> -->
                            </figure>
                            <div class="product-details">
                                <div class="product-cat">
                                    <!-- <a href="shop-grid-3cols.html">Clothing</a> -->
                                </div>
                                <h3 class="product-name">
                                <p><a href="">{{$new->name}}</a></p>
                                </h3>
                                <div class="product-price">
                                    <ins class="new-price">{{$new->oprize}}</ins><del class="old-price">{{$new->aprize}}</del>
                                </div>
                               
                              
                            </div>
                        </div>
                        @endforeach
                        
                        
                    </div>
                </section>

                

                <section class="product-wrapper mt-6 mt-md-10 pt-4 mb-10 pb-2 container appear-animate" data-animation-options="{
                    'delay': '.6s'
                }">
                    <h2 class="title title-center">Our Products</h2>
                    <div class="owl-carousel owl-theme row cols-2 cols-md-3 cols-lg-4 cols-xl-5" data-owl-options="{
                        'items': 5,
                        'nav': false,
                        'loop': false,
                        'dots': true,
                        'margin': 20,
                        'responsive': {
                            '0': {
                                'items': 2
                            },
                            '768': {
                                'items': 3
                            },
                            '992': {
                                'items': 4
                            },
                            '1200': {
                                'items': 5,
                                'dots': false,
                                'nav': true
                            }
                        }
                    }">
                        
                       
                       @foreach($normal as $norm)
                        <div class="product text-center">
                            <figure class="product-media">
                                <a href="product.html">
                                    <img src="uploads/products/{{$norm->image}}" alt="Blue Pinafore Denim Dress"
                                        width="220" height="245" style="background-color: #f2f3f5;" />
                                </a>
                                
                            </figure>
                            <div class="product-details">
                                <!-- <div class="product-cat">
                                    <a href="shop-grid-3cols.html">Bags & Backpacks</a>
                                </div> -->
                                <h3 class="product-name">
                                <p><a href="">{{$norm->name}}</a></p>
                                </h3>
                                <div class="product-price">
                                    <ins class="new-price">{{$norm->oprize}}</ins><del class="old-price">{{$norm->aprize}}</del>
                                </div>
                               
                            </div>
                        </div>
                     @endforeach
                        
                    </div>
                </section>

             
            </div>
        </main>

      
   @endsection