@extends('layouts.users')
@section('content')
<main class="main">
            <nav class="breadcrumb-nav">
            </nav>
            <div class="page-content with-sidebar">
                <div class="container">
                    <div class="row gutter-lg">
                        <div class="col-lg-9">
                            <div class="posts grid post-grid row" data-grid-options="{
                                'layoutMode': 'fitRows'
                            }">


                            @foreach($products as $prod)
                                <div class="grid-item col-sm-4">
                                    <article class="post mb-3">
                                        <figure class="post-media overlay-zoom">
                                            <a href="">
                                                <img src="/uploads/products/{{$prod->image}}" width="430" height="300"
                                                    alt="post" />
                                            </a>
                                        </figure>
                                        <div class="post-details">
                                            <p><a href="">{{$prod->name}}</a></p>
                                            <div class="product-price">
												<ins class="new-price">{{$prod->oprize}}</ins><del class="old-price">{{$prod->aprize}}</del>
											</div>
                                           
                                        </div>
                                    </article>
                                </div>
                               @endforeach
                               
                               
                             
                             
                            </div>
                         
                        </div>
                        <aside class="col-lg-3 right-sidebar sidebar-fixed sticky-sidebar-wrapper">
                           
                            <div class="sidebar-content">
                                <div class="sticky-sidebar" data-sticky-options="{'top': 89, 'bottom': 70}">
                                    <!-- <div class="widget widget-search border-no mb-2">
                                        <form action="#" class="input-wrapper input-wrapper-inline btn-absolute">
                                            <input type="text" class="form-control" name="search" autocomplete="off"
                                                placeholder="Search in Blog" required />
                                            <button class="btn btn-search btn-link" type="submit">
                                                <i class="d-icon-search"></i>
                                            </button>
                                        </form>
                                    </div> -->
                                    <div class="widget widget-collapsible border-no">
                                        <h3 class="widget-title">Product Categories</h3>
                                        <ul class="widget-body filter-items search-ul">

                                           @foreach($cat as $ca)
                                           <li><a href="/productbycategory/{{$ca->id}}">{{$ca->name}}</a></li>
                                           @endforeach
                                          
                                            
                                            
                                        </ul>
                                    </div>
                                  
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </main>
@endsection