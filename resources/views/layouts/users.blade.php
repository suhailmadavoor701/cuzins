<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Cuzins </title>

    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="D-THEMES">

    <!-- Favicon -->
   <style>
       Minipopup-area  {
    visibility: hidden;
        }

        .minipopup-box   {
    visibility: hidden;
        }
   </style>


  </script>
    <!-- <script>
        WebFontConfig = {
            google: { families: [ 'Poppins:300,400,500,600,700,800' ] }
        };
        ( function ( d ) {
            var wf = d.createElement( 'script' ), s = d.scripts[ 0 ];
            wf.src = 'js/webfont.js';
            wf.async = true;
            s.parentNode.insertBefore( wf, s );
        } )( document );
    </script> -->



    <link rel="stylesheet" type="text/css" href="{{URL::asset('users/vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('users/vendor/animate/animate.min.css')}}">

    <!-- Plugins CSS File -->
    <link rel="stylesheet" type="text/css" href="{{asset('users/vendor/magnific-popup/magnific-popup.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('users/vendor/owl-carousel/owl.carousel.min.css')}}">

    <!-- Main CSS File -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('users/css/demo1.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('users/css/style.min.css')}}">

    <link rel="stylesheet" href="{{ asset('users/css/frontend.css') }}">
</head>

<body class="home">

    <div class="page-wrapper">
        <!-- <h1 class="d-none">Riode - Responsive eCommerce HTML Template</h1> -->
        <header class="header">
            <div class="header-top">
                <div class="container">
                    <div class="header-left">
                        <p class="welcome-msg">Welcome to Cuzins Shop from Here!</p>
                    </div>
                    
                </div>
            </div>
            <!-- End HeaderTop -->
            <div class="header-middle sticky-header fix-top sticky-content">
                <div class="container">
                    <div style="padding-right: 53px;" class="header-left">
                        <a href="#" class="mobile-menu-toggle">
                            <i class="d-icon-bars2"></i>
                        </a>
                        <a   href="demo1.html" class="logo">
                            <img style="width:306;height:px;"  src="{{URL::asset('users/images/logo.png')}}" alt="logo"  />
                        </a>
                        <!-- End Logo -->

                        <!-- <div class="header-search hs-simple">
                            <form action="#" class="input-wrapper">
                                <input type="text" class="form-control" name="search" autocomplete="off"
                                    placeholder="Search..." required />
                                <button class="btn btn-search" type="submit">
                                    <i class="d-icon-search"></i>
                                </button>
                            </form>
                        </div> -->
                        <div class="header-bottom d-lg-show">
                            <div class="container">
                        <div class="header-left">
                            <nav class="main-nav">
                                <ul class="menu">
                                    <li class="active">
                                        <a href="/">Home</a>
                                    </li>
                                    <li>
                                        <a href="/usersproducts">Products</a>
                                    </li>
             <li>
                    <a href="#">Categories</a>
                    <ul>
                        @foreach($categories as $cat)
                    <li><a href="/productbycategory/{{$cat->id}}">{{$cat->name}}</a></li>
                    @endforeach
                     
                    </ul>
                </li>
                                    
                                    <!-- <li>
                                        <a href="#">About Us</a>
                                    </li>
                                    <li> -->
                                        <a href="/contact">Contact Us</a>
                                    </li>
                                            
                                           
                                        </ul>
                                    
                                   
                                
                            </nav>
                        </div>
                        </div>
                        </div>
                    </div>
                
                </div>
            </div>

            <!-- <div class="header-bottom d-lg-show">
                <div class="container">
                    <div class="header-left">
                        <nav class="main-nav">
                            <ul class="menu">
                                <li class="active">
                                    <a href="demo1.html">Home</a>
                                </li>
                                <li>
                                    <a href="#">Products</a>
                                </li>
                                
                                <li>
                                    <a href="#">About Us</a>
                                </li>
                                <li>
                                    <a href="#">Contact Us</a>
                                </li>
                                        
                                       
                                    </ul>
                                
                               
                            
                        </nav>
                    </div>
                    
                </div>
            </div> -->
        </header>
        <!-- End Header -->


   @yield('content')

     <!-- End of Main -->
     <footer class="footer">
            <div class="container">
                <div class="footer-top">
                  
                </div>
                <!-- End FooterTop -->
                <div class="footer-middle">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="widget widget-info">
                                <h4 class="widget-title">Contact Info</h4>
                                <ul class="widget-body">
                                    <li>
                                        <label>Mobile:</label>
                                        <a target="_blank" href="tel:919745177722">974 5177 722</a>
                                    </li>
                                    <li>
                                        <label>Email:</label>
                                        <a target="_blank" href="mailto:cuzinskunnamangalam1@gmail.com">cuzinskunnamangalam1@gmail.com</a>
                                    </li>
                                    <li>
                                        <label>Address:</label>
                                        <a href="#">Kunnamangalam, Karanthur, Oppo:Palakkal Petrol Pumb</a>
                                    </li>
                                    <li>
                                        <label>WORKING DAYS / HOURS:</label>
                                    </li>
                                    <li>
                                        <a href="#">Mon - Sun / 9:00 AM - 9:00 PM</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- End Widget -->
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="widget ml-lg-4">
                                <h4 class="widget-title">Quik Links</h4>
                                <ul class="widget-body">
                                    <!-- <li>
                                        <a href="">About Us</a>
                                    </li> -->
                                    <li>
                                        <a href="/contact">Contact</a>
                                    </li>
                                    <li>
                                        <a href="products">Products</a>
                                    </li>
                                    <!-- <li>
                                        <a href="#">About Us</a>
                                    </li> -->
                                   
                                </ul>
                            </div>
                            <!-- End Widget -->
                        </div>
                       
                        
                    </div>
                </div>
                <!-- End FooterMiddle -->
                <div class="footer-bottom">
                    <div class="footer-left">
                        <figure class="payment">
                            <img src="{{URL::asset('users/images/payment.png')}}" alt="payment" width="159" height="29" />
                        </figure>
                    </div>
                    <div class="footer-center">
                        <p class="copyright">Risbuzz &copy; 2021. All Rights Reserved</p>
                    </div>
                    <div class="footer-right">
                        <div class="social-links">
                            <a href="#" class="social-link social-facebook fab fa-facebook-f"></a>
                            <a href="#" class="social-link social-twitter fab fa-twitter"></a>
                            <a href="#" class="social-link social-linkedin fab fa-linkedin-in"></a>
                        </div>
                    </div>
                </div>
                <!-- End FooterBottom -->
            </div>
        </footer>
        <!-- End Footer -->
    </div>
    
 <!-- Scroll Top -->
 <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="d-icon-arrow-up"></i></a>

<!-- MobileMenu -->
<div class="mobile-menu-wrapper">
	<div class="mobile-menu-overlay">
	</div>
	<!-- End of Overlay -->
	<a class="mobile-menu-close" href="#"><i class="d-icon-times"></i></a>
	<!-- End of CloseButton -->
	<div class="mobile-menu-container scrollable">
	   
		<!-- End of Search Form -->
		<ul class="mobile-menu mmenu-anim">
			<li class="active">
				<a href="/">Home</a>
			</li>
			<li>
				<a href="/products">Products</a>
			</li>
			
            <li>
                    <a href="#">Categories</a>
                    <ul>
                        @foreach($categories as $cat)
                    <li><a href="/productbycategory/{{$cat->id}}">{{$cat->name}}</a></li>
                    @endforeach
                     
                    </ul>
                </li>
			<li>
				<a href="/contact">Contact Us</a>
			</li>
					
		</ul>
		<!-- End of MobileMenu -->
		<!-- <ul class="mobile-menu mmenu-anim">
			<li><a href="login.html">Login</a></li>
			<li><a href="cart.html">My Cart</a></li>
			<li><a href="wishlist.html">Wishlist</a></li>
		</ul> -->
		<!-- End of MobileMenu -->
	</div>
</div>



<!-- Plugins JS File -->
<script src="{{URL::asset('users/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{URL::asset('users/vendor/parallax/parallax.min.js')}}"></script>
<script src="{{URL::asset('users/vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{URL::asset('users/vendor/elevatezoom/jquery.elevatezoom.min.js')}}"></script>
<script src="{{asset('users/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{URL::asset('users/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
<!-- Main JS File -->
<script src="{{URL::asset('users/js/main.min.js')}}"></script>
</body>


</html>