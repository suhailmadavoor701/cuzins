@extends('layouts.admin')

<br>
@section('content')


    @section('content')


    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


          @if(Session::has('status'))
<p class="alert alert-info">{{ Session::get('status') }}</p>
@endif

 <section class="content-header">
      <h1>
        Product Editing
        {{-- <small>preview of simple tables</small> --}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="\home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product</a></li>
        <li class="active">Editing</li>
      </ol>
    </section>
 
   <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">

        <div class="box box-primary">
            <div class="box-header with-border">
              {{-- <h3 class="box-title">Banner Uploading</h3> --}}
            </div>
            <!-- /.box-header -->
            <!-- form start -->
          

    <form method="post" action="/product/{{$id}}" enctype="multipart/form-data">
              {{csrf_field()}}
               <input name="_method" type="hidden" value="PATCH">
              <div class="box-body">
                <div class="form-group">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Category</label>
                                    <select class="form-control" name="name">
                                      <option class="form-control" >Select The Category</option>
                                      @foreach($table as $item)




                                      <option value="{{ $item->id }}" selected>{{ $item->name }} </option> 

                                        {{-- <option required="" name="name"  class="form-control" value="{{$item->id}}">{{$item->name}}</option> --}}
                                      @endforeach
                                 </select>
                             </div>
                </div>
               <div class="form-group">
                  <label>Name of Product</label>
                  <input type="text" name="pname" required="" value="{{ $product->name }}" class="form-control" placeholder="Enter name">
                </div>
                <div class="form-group">
                  <label>Offer Prize</label>
                  <input type="text" name="oprize" required="" value="{{ $product->oprize }}" class="form-control" placeholder="Enter Offer Prize">
                </div>
                <div class="form-group">
                  <label>Actual Prize</label>
                  <input type="number" name="aprize" required="" value="{{ $product->aprize }}" class="form-control" placeholder="Enter Actual Prize">
                </div>
                <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" name="description" value="{{ $product->description }}" required="" rows="3" 
                  placeholder="Enter description">{{ $product->description }}"</textarea>
                </div>
             
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" required="" name="image" >

                  {{-- <p class="help-block">Example block-level help text here.</p> --}}
                </div> 
              
              
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
      </div>
  </div>
</section>

@endsection





