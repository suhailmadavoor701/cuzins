@extends('layouts.tables')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Products Table
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Products List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
              <!-- <table class="table table-hover"> -->
                <tr>
                  <th>Category</th>
                  <th>Section</th>
                  <th>Name</th>
                  <th>Actual Prize</th>
                  <th>Offer Prize</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th colspan="2" style="text-align: center;">Action</th>
                </tr>
                @foreach($banner as $ban)
                 <tr>
                 <td>{{ $ban->cat }}</td>
                 <td>{{ $ban->section }}</td>
                 <td>{{ $ban->name }}</td>
                  <td>{{ $ban->aprize }}</td>
                  <td>{{ $ban->oprize }}</td>
                  
                  <td>{{ $ban->description }}</td>
                  
                  

                   <td><img height="50px" width="50px" src="{{ asset('uploads/products/'.$ban->image) }}"></td>
                  
                <td><a href="/product/{{ $ban->id }}/edit" class="btn btn-warning">Edit</a></td>

                              <td>
                                          <form action="/product/{{ $ban->id }}" method="post">
                                            {{csrf_field()}}
                                            <input name="_method" type="hidden" value="DELETE">
                                           <button onclick="if (confirm('Are you sure..?')) commentDelete(1); return false" class="btn btn-warning"  type="submit">Delete</button>
                                          </form>

                                        </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

         
  

@endsection