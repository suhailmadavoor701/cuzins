<?php

namespace App\Http\Controllers;
use App\Contracts\OrderContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\UserTrait;
use Cart;

class checkoutController extends Controller
{    

    public function index()
    {

     $cartitems = Cart::session(auth()->id())->getContent();
     return view('users.check',compact('cartitems'));
    }
    
}
