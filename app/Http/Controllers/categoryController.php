<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Category;
use Validator;
use File;
class categoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $banner=DB::select('select * from categories order by id DESC');
        return view('category.index',compact('banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), [
        'name' => 'required|max:100',
        'image' => 'required|mimes:jpeg,jpg,png',
        
    ]);

    if ($validator->fails()) {
        return redirect('/category/index')
                    ->withErrors($validator)
                    ->withInput();
    }

    

            $image = new Category();
            if ($request->hasFile('image')) {
                $dir = 'uploads/category/';
                $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
                $fileName = str_random() . '.' . $extension; // rename image
                $request->file('image')->move($dir, $fileName);

                $image->image = $fileName;
            $image->name= $request->name;
            }
          
            $image->save();
        
        return redirect()->back()->with('status','Successfully Uploaded Category.Thank you');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $turf=Category::find($id);

         $image_path =public_path("uploads/category/$turf->image");
    if (File::exists($image_path)) {
        //File::delete($image_path);
        unlink($image_path);
    }
       
       $turf->delete();
       return redirect()->back()->with('status','Successfully Deleted.Thank you');
    }
}
