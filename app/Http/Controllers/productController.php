<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Validator;
use App\Models\Product;
use DB;
use File;

class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $category=DB::select('select * from categories order by id DESC');
        //  $banner=DB::select('select * from products order by id DESC');
        $banner= DB::table('products')
        ->join('categories', 'categories.id', '=', 'products.categories_id')
        ->select('products.*', 'categories.name as cat')
        ->orderBy('id', 'desc')
        ->get();
        // dd($banner);


        return view('product.index',compact('banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
         $category = Category::all(['id', 'name']);
        $table = Category::orderBy('id','DESC')->get();
 
        return view('product.create',compact('category',$category,'table'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), [
        'name' => 'required|max:50',
        'image' => 'required',
        'description' => 'required|max:500',
        
        'oprize' => 'required',
        'aprize' => 'required',
        


    ]);

      if ($validator->fails()) {
            return redirect('/product/create')
                        ->withErrors($validator)
                        ->withInput();
        }
    

             $image = new Product();
            if ($request->hasFile('image')) {
                $dir = 'uploads/products/';
                $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
                $fileName = str_random() . '.' . $extension; // rename image
                $request->file('image')->move($dir, $fileName);

                $image->image = $fileName;
                $image->categories_id = $request->name;
                $image->name = $request->pname;
                
                $image->description = $request->description;
                $image->oprize= $request->oprize;
                $image->aprize= $request->aprize;
                $image->section= $request->section;
                // dd($image);
                
                

                          $image->save();


         }
                


        return redirect('/product')->with('status','Successfully Uploaded Product .Thank you');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product::find($id);
        $table=DB::select('select * from categories order by id DESC');
        return view('product.edit',compact('product','id','table'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = Validator::make($request->all(), [
        'name' => 'required|max:50',
        'image' => 'required',
        'description' => 'required|max:500',
        
        'oprize' => 'required',
        'aprize' => 'required',
        


    ]);

      if ($validator->fails()) {
            return redirect('/product/create')
                        ->withErrors($validator)
                        ->withInput();
        }
    

             $image=Product::find($id);
            if ($request->hasFile('image')) {
                $dir = 'uploads/products/';
                $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
                $fileName = str_random() . '.' . $extension; // rename image
                $request->file('image')->move($dir, $fileName);

                $image->image = $fileName;
                $image->categories_id = $request->name;
                $image->name = $request->pname;
                
                $image->description = $request->description;
                $image->oprize= $request->oprize;
                $image->aprize= $request->aprize;
                

                          $image->save();



         }
                


         return redirect('/product')->with('status','Successfully Uploaded Product .Thank you');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $turf = Product::find($id);
         $image_path= public_path('uploads/products/'.$turf->image);
         if (File::exists($image_path)) {
            unlink($image_path);
        }
       $turf->delete();
       return redirect()->back()->with('status','Successfully Deleted.Thank you');
    }
    
}
