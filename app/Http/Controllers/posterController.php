<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Poster;
use Validator;

class posterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner=DB::select('select * from posters order by id DESC');
        return view('poster.index',compact('banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            
            'image' => 'required|mimes:jpeg,jpg,png',
            
        ]);
    
        if ($validator->fails()) {
            return redirect('/posters/index')
                        ->withErrors($validator)
                        ->withInput();
        }
    
        
    
                $image = new Poster();
                if ($request->hasFile('image')) {
                    $dir = 'uploads/posters/';
                    $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
                    $fileName = str_random() . '.' . $extension; // rename image
                    $request->file('image')->move($dir, $fileName);
    
                    $image->image = $fileName;
                }
              
                $image->save();
            
            return redirect()->back()->with('status','Successfully Uploaded Category.Thank you');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
