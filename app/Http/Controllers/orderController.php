<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;

class orderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $request->validate([
            'name' => 'required',
            'state' => 'required',
            'city' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'post' => 'required',
            'payment_method' => 'required',
        ]);

        $order = new Order();

        $order->order_number = uniqid('OrderNumber-');

        $order->shipping_fullname = $request->input('name');
        $order->shipping_state = $request->input('state');
        $order->shipping_city = $request->input('city');
        $order->shipping_address = $request->input('address');
        $order->shipping_phone = $request->input('phone');
        $order->shipping_zipcode = $request->input('post');

        if(!$request->has('billing_fullname')) {
                      $order->billing_fullname = $request->input('name');
            $order->billing_state = $request->input('state');
            $order->billing_city = $request->input('city');
            $order->billing_address = $request->input('address');
            $order->billing_phone = $request->input('phone');
            $order->billing_zipcode = $request->input('post');
        }else {
            $order->billing_fullname = $request->input('name');
            $order->billing_state = $request->input('state');
            $order->billing_city = $request->input('city');
            $order->billing_address = $request->input('baddress');
            $order->billing_phone = $request->input('phone');
            $order->billing_zipcode = $request->input('post');
        }
        


       $order->grand_total = \Cart::session(auth()->id())->getTotal();
        $order->item_count = \Cart::session(auth()->id())->getContent()->count();

        $order->user_id = auth()->id();

        if (request('payment_method') == 'paypal') {
            $order->payment_method = 'paypal';
        }

    
        $order->save();

        //save order items

       $cartItems = \Cart::session(auth()->id())->getContent();

        foreach($cartItems as $item) {
            $order->items()->attach($item->id, ['price'=> $item->price, 'quantity'=> $item->quantity,'size'=> $item->attributes->size]);
        }



         //payment
        if(request('payment_method') == 'paypal') {
                //redirect to paypal
            return redirect()->route('paypal.checkout', $order->id);

        }




        

        

        //empty cart
        \Cart::session(auth()->id())->clear();
        //send email to customer


        return redirect()->back()->with('status','Successfully Submitted Your Order .Thank you');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
