<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Models\Product;

class cartController extends Controller
{

    public function addToCart($id)
{   

        	$cartConditions = Cart::getConditions();

            $product=Product::find($id);

          Cart::session(auth()->id())->add(array(
    'id' => $id,
            'name' => $product->name,
            'price' => $product->oprize,
    'quantity' =>'1',
     'attributes'=>array(

              'size'=>'small',
              'image'=>$product->image,
            ),
));
        
               return back()->with('success','Added to Cart Succesfully');

}


		public function index()
		{


        $cartitems = Cart::session(auth()->id())->getContent();



        
        

        return view('users.cart',compact('cartitems'));
		}



   public function destroy($id)
        {
            Cart::session(auth()->id())->remove($id);
            return back();
        }

        public function clear()

        {

            Cart::clear();
            return back();

        }




public function update(Request $request,$id)
    {

        $product=Product::find($id);

                  Cart::session(auth()->id())->update($id, array(
            'quantity' => array(
                'relative' => false,
                'value' => $request->qty,
                'size'=>$request->size,
            ),

            'attributes'=>array(

              'size'=>$request->size,
              'image'=>$product->image,
            ),


          ));
          
          $cartItems = Cart::getContent();
           
                 return back();
        }



}