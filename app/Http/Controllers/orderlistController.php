<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;

class orderlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order =Order::find($id);

        // foreach ($order->items as $value) {

        //  dd($value->pivot->quantity);
            
        // }

        return view('order.orderview',compact('order'));
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $order=Order::find($id);
          $order = $request->input('name');
          if($order=='completed')
          {
            $order=Order::find($id);
             $order->status = 'completed';
           $order->save();
            return redirect('/itemorders')->with('status','Successfully Submitted  .Thank you');

          }
          else
          {
            $order=Order::find($id);

             $order->status = 'pending';


           $order->save();
            return redirect('/itemorders')->with('status','Successfully Submitted  .Thank you');

          }
         
         

        
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
                 $turf=Order::find($id);

       $turf->delete();
       return redirect()->back()->with('status','Successfully Deleted.Thank you');
    }
        



}
