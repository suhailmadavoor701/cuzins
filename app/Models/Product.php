<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

     protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = ['image','name','aprize','oprize','description','categories_id','section'];

    public function images(){
    	
        return $this->belongsTo('App\Models\Category','id');
    }

    public function items()
    {
    	return $this->belongsToMany(orderitems::class);
    }
}
