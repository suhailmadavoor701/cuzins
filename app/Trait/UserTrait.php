<?php

namespace App\Traits;
use App\Models\User;

trait UserTrait {


    public function index() {
       
        $users = User::all();
        return view('users')->with(compact('users'));
    }
}